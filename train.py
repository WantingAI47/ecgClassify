import tensorflow as tf
import model
from tensorflow.python.framework import graph_util
from sklearn.model_selection import train_test_split
import pandas as pd
import dataaug

from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN

pb_file_path = 'E:/ecgdata/heartbeat/classify.pb'
# train_path = 'E:/ecgdata/heartbeat/mitbih_train.csv'
test_path = 'E:/ecgdata/heartbeat/mitbih_test.csv'

# A0, A1, A2, A3, A4 = preprossdata.prodata(train_path)
#



def oversampling(X_train, Y_train):
    sampler = ADASYN()
    x_resampled, y_resampled = sampler.fit_sample(X_train,Y_train)
    return x_resampled,y_resampled

x = tf.placeholder(tf.float32, shape=[None, 187], name='inx')
y = tf.placeholder(tf.float32, shape=[None, 5])
drop_prob_ = tf.placeholder(tf.float32, name='drop')
learning_rate_ = tf.placeholder(tf.float32, name='learning_rate')
is_training = tf.placeholder(tf.bool, name='is_training')
epochs = 20
train_path = 'E:/ecgdata/heartbeat/mitbih_train.csv'
ss = pd.read_csv(train_path)
mm = ss.values[:,0:187]
nn = ss.values[:,187]
xx, yy = oversampling(mm, nn)


testdata = pd.read_csv(test_path)
x_test = testdata.values[:,0:187]
y_test = testdata.values[:,187]



def one_hot(labels):
    sess = tf.Session()
    batch_size = tf.size(labels)
    labels = tf.expand_dims(labels, 1)
    indices = tf.expand_dims(tf.range(0, batch_size, 1), 1)
    concated = tf.concat([indices, labels], 1)
    onehot_labels = tf.sparse_to_dense(concated, tf.stack([batch_size, 5]), 1, 0)
    xx = sess.run(onehot_labels)
    return xx


logits, regularization_loss = model.resnet(x, drop_prob_,is_training)
with tf.name_scope('cost'):
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y)) + regularization_loss
    tf.summary.scalar('cost', cost)
optimizer = tf.train.AdamOptimizer(learning_rate_).minimize(cost)
label_pr = tf.argmax(logits, 1, name='prlabel')
# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
with tf.name_scope('accuracy'):
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name='accuracy')
    tf.summary.scalar('accuracy', accuracy)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    total_step = 0
    for e in range(epochs):
        train_data, test_data, train_lables, test_lables = train_test_split(xx, yy, test_size=0)
        for iteration in range(5661):
            total_step += 1
            inputx, inputy =dataaug.get_batches(iteration,train_data,train_lables,batch_size=64)
            inputyy = [int(x) for x in inputy]
            inputy1 = one_hot(inputyy)
            feed = {x: inputx, y: inputy1, drop_prob_: 0.5, learning_rate_: 0.0001, is_training: True}
            loss, _, acc = sess.run([cost, optimizer, accuracy], feed_dict=feed)
            # if total_step % 5 == 0:
            #     summary_str = sess.run(merged)
            #     writer.add_summary(summary_str, total_step)
            if iteration % 5 == 0:
                print("Epoch: {}/{}".format(e, epochs), "Iteration: {:d}".format(iteration),
                      "Train loss: {:6f}".format(loss), "Train acc: {:.6f}".format(acc))
                # Compute validation loss at every 10 iterations
                if iteration % 10 == 0:
                    ys = [int(x) for x in y_test]
                    valy = one_hot(ys)
                    feed = {x: x_test, y: valy, drop_prob_: 0, is_training: False}
                    loss_v, acc_v, pl = sess.run([cost, accuracy, label_pr], feed_dict=feed)
                    print(pl)
                    # confusion_matrixtest = confusion_matrix(test_lables, pl)
                    # F1 = f1_score(test_lables, pl)
                    print("Epoch: {}/{}".format(e, epochs), "Iteration: {:d}".format(iteration),
                          "Validation loss: {:6f}".format(loss_v),
                          "Validation acc: {:.6f}".format(acc_v))  # SEQ_LEN = 188
    constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph_def,
                                                               ["inx", 'prlabel', 'keep'])
    with tf.gfile.FastGFile(pb_file_path, mode='wb') as f:
        f.write(constant_graph.SerializeToString())

