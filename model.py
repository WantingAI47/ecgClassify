import tensorflow as tf


def resnet(x, droprate,is_traing=True):
    x = tf.reshape(x, [-1, 187, 1])
    conv1 = tf.layers.conv1d(inputs=x, filters=32, kernel_size=5, strides=1, padding='same', activation=None,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv1')
    conv2 = tf.layers.conv1d(inputs=conv1, filters=32, kernel_size=5, strides=1, padding='same', activation=tf.nn.relu,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv2')
    conv3 = tf.layers.conv1d(inputs=conv2, filters=32, kernel_size=5, strides=1, padding='same', activation=None,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv3')
    conv3_bn = tf.layers.batch_normalization(conv3, training=is_traing)
    block1 = conv1 + conv3_bn
    nice1 = tf.nn.relu(block1)
    out1 = tf.layers.max_pooling1d(inputs=nice1, pool_size=5, strides=2)
    conv4 = tf.layers.conv1d(inputs=out1, filters=32, kernel_size=5, strides=1, padding='same', activation=tf.nn.relu,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv4')
    conv5 = tf.layers.conv1d(inputs=conv4, filters=32, kernel_size=5, strides=1, padding='same', activation=None,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv5')
    conv5_bn = tf.layers.batch_normalization(conv5, training=is_traing)
    block2 = out1 + conv5_bn
    nice2 = tf.nn.relu(block2)
    out2 = tf.layers.max_pooling1d(inputs=nice2, pool_size=5, strides=2)
    conv6 = tf.layers.conv1d(inputs=out2, filters=32, kernel_size=5, strides=1, padding='same', activation=tf.nn.relu,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv6')
    conv7 = tf.layers.conv1d(inputs=conv6, filters=32, kernel_size=5, strides=1, padding='same', activation=None,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv7')
    conv7_bn = tf.layers.batch_normalization(conv7, training=is_traing)
    block3 = out2 + conv7_bn
    nice3 = tf.nn.relu(block3)
    out3 = tf.layers.max_pooling1d(inputs=nice3, pool_size=5, strides=2)
    conv8 = tf.layers.conv1d(inputs=out3, filters=32, kernel_size=5, strides=1, padding='same', activation=tf.nn.relu,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv8')
    conv9 = tf.layers.conv1d(inputs=conv8, filters=32, kernel_size=5, strides=1, padding='same', activation=None,
                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                             kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv9')
    conv9_bn = tf.layers.batch_normalization(conv9, training=is_traing)
    block4 = out3 + conv9_bn
    nice4 = tf.nn.relu(block4)
    out4 = tf.layers.max_pooling1d(inputs=nice4, pool_size=5, strides=2)
    conv10 = tf.layers.conv1d(inputs=out4, filters=32, kernel_size=5, strides=1, padding='same', activation=tf.nn.relu,
                              kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                              kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv10')
    conv11 = tf.layers.conv1d(inputs=conv10, filters=32, kernel_size=5, strides=1, padding='same', activation=None,
                              kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                              kernel_regularizer=tf.contrib.layers.l2_regularizer(0.009), name='conv11')
    conv11_bn = tf.layers.batch_normalization(conv11, training=is_traing)
    block5 = out4 + conv11_bn
    nice5 = tf.nn.relu(block5)
    out5 = tf.layers.max_pooling1d(inputs=nice5, pool_size=5, strides=2)
    flatten = tf.reshape(out5, [-1, 64])
    dense = tf.layers.dense(inputs=flatten, units=32, activation=tf.nn.relu)

    dense = 5 * tf.divide(dense, tf.norm(dense, ord='euclidean'))
    dropout = tf.layers.dropout(inputs=dense, rate=droprate)

    logits = tf.layers.dense(inputs=dropout, units=5)

    regularization_loss = tf.reduce_sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
    return logits, regularization_loss
