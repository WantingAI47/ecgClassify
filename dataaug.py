import random
import numpy as np
from collections import Counter
import pandas as pd

from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN

length = 187




def oversampling(X_train, Y_train):
    sampler = ADASYN()
    x_resampled, y_resampled = sampler.fit_sample(X_train,Y_train)
    return x_resampled,y_resampled




def get_batches(iterations, x, y, batch_size):
    length = x.shape[0]
    batches_wavs = []
    batches_labels = []
    if iterations * batch_size < length:
        for j in range(batch_size):
            dsd = x[(iterations - 1) * batch_size + j]
            batches_wavs.append(dsd)
            batches_labels.append(y[(iterations - 1) * batch_size + j])
    else:
        for j in range(batch_size):
            dsd = x[(iterations - 1) * batch_size + j - length]
            batches_wavs.append(dsd)
            batches_labels.append(y[(iterations - 1) * batch_size + j - length])
    return batches_wavs, batches_labels


